## WebView Debug Sample

[TOC levels=3,6]: # "Version History"

### Version History
- [0.5.12](#0512)
- [0.5.10](#0510)
- [0.5.8](#058)
- [0.5.6](#056)


### 0.5.12

* update to latest JavaFx-WebView-Debugger dependency

### 0.5.10

* update to latest JavaFx-WebView-Debugger dependency

### 0.5.8

* Add: Reload Page & Pause action

### 0.5.6

First working maven version

